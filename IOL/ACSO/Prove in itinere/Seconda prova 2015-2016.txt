Si consideri l'achitettura MIC-1. Si microprogammi l'istruzione RANK (indirizzo 0x01) che ordini in maniera crescente (verso l’alto della pila) i 4 valori in cima alla pila.

Esempi:

(fondo della pila, valori …, cima della pila)

(1,1,1,1) -> (1,1,1,1)
(78,2,2,2) -> (2,2,2,78)
(78,2,78,78) -> (2,78,78,78)
(31,31,14,14) -> (14,14,31,31)
(64,83,17,1) -> (1,17,64,83)
(64,1,17,83) -> (1,17,64,83)
(17,64,1,83) -> (1,17,64,83)

N.B. consegnare il file contenente tutte le istruzioni di default (contenute nel file mic1ijvm.mal) più quella da voi sviluppata. Il file deve essere nominato nel seguente modo (chi ha più nomi/cognomi può usare solo il primo): "cognome_nome_matricola.mal".