﻿Si scriva un programma IJVM che implementi la seguente funzionalità:
 
- sulla pila sono presenti (inseriti tramite BIPUSH) 'n' valori compresi tra 1 e 127, preceduti dal valore 0;
 
- il numero 'n' di valori presenti può variare da 2 a 20;
 
- il programma lascia sulla pila soltanto la distanza più grande (non in valore assoluto) tra tutti i valori consecutivi (escluso lo 0) presenti sulla pila.


Esempi di funzionamento ([pila in input] -> [pila in output])
:

[0, 1, 3] -> [2]

[0, 5, 6] -> [1]

[0, 94, 3, 10, 20] -> [10]

[0, 1, 118, 120] -> [117]

[0, 76, 76] -> [0]

[0, 5, 17, 19] -> [12]

[0, 100, 87, 6, 15, 83, 27] -> [68]

[0, 100, 87, 6, 15, 93, 27] -> [78]

[0, 100, 17, 6, 15, 3, 121, 18] -> [118]

[0, 100, 17, 6, 1, 3, 121, 18] -> [118]